defmodule BullsAndCows do
  def score_guess(secret, guess) do
    d = String.to_charlist(guess)
    s = String.to_charlist(secret)
    max = length(s)

    bulls = Enum.map(0..max, fn(x) -> if(Enum.at(d, x) == Enum.at(s, x) and x < length(s)) do 1 else 0 end end)

    c = for x <- 0..max do
      if(Enum.member?(s, Enum.at(d, x)) and Enum.at(d, x) != Enum.at(s, x)) do
        1
      else
        0
      end
    end

    ba = Enum.sum(bulls)
    cowsA = Enum.sum(c)

    if(ba == 4 or cowsA == 4) do
      "You win"
    else
      "#{ba} Bulls, #{cowsA} Cows"
    end
  end
end
  # def score_guess(secret, guess) do
  #   "0 Bulls, 0 Cows"
  # end

  # def score_guess(secret, guess) do
  #   d = String.to_charlist(guess)
  #   s = String.to_charlist(secret)
  #   a = for x <- d do
  #     if(Enum.member?(s, x)) do
  #       1
  #     else
  #       0
  #     end
  #   end
  #   c = Enum.sum(a)
  #   "0 Bulls, #{c} Cows"
  # end

  # def score_guess(secret, guess)do

  # end
